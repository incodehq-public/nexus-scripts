import groovy.json.JsonOutput
import groovy.json.JsonSlurper
import org.sonatype.nexus.repository.maven.MavenHostedFacet
import org.sonatype.nexus.repository.storage.Bucket
import org.sonatype.nexus.repository.storage.StorageFacet

import java.util.regex.Matcher

def request = new JsonSlurper().parseText(args)
assert request.repo: 'repo parameter is required'


def repo = repository.repositoryManager.get(request.repo)
//log.info("repo: ${repo}")

StorageFacet storageFacet = repo.facet(StorageFacet)
MavenHostedFacet mavenHostedFacet = repo.facet(MavenHostedFacet)


def repoTx = storageFacet.txSupplier().get()

log.info("request.group  : ${request.group}")
log.info("request.name   : ${request.name}")
log.info("request.version: ${request.version}")
log.info("request.execute: ${request.execute}")

Date beforeDate = null
if(request.before) {
    Matcher matcher = request.before =~ /.*([0-9]{8}).*/
    if(matcher.matches()) {
        def datePart = matcher.group(1)
        try {
            beforeDate = Date.parse("yyyyMMdd", datePart)
        } catch (Exception ignore) {
            beforeDate = null
        }
    }
}
log.info("beforeDate: ${beforeDate}")

def gavs = []
def deletedVersions = []

def componentList = []

try {
    repoTx.begin()
    //log.info("repoTx: ${repoTx}")

    Bucket bucket = repoTx.findBucket(repo)
    //log.info("bucket: ${bucket}")

    def components = repoTx.browseComponents(bucket)

    components.each { component ->

        boolean include =
                (request.group   == "" || component.group()   =~ /${request.group}/   ) &&
                (request.name    == "" || component.name()    =~ /${request.name}/    ) &&
                (request.version == "" || component.version() =~ /${request.version}/ )

        if(include && beforeDate != null) {
            Matcher matcher = component.version =~ /.*([0-9]{8}).*/
            if(matcher.matches()) {
                def datePart = matcher.group(1)
                try {
                    def componentDate = Date.parse("yyyyMMdd", datePart)
                    include = componentDate < beforeDate
                } catch (Exception ignored) {
                }
            }
        }

        if(include) {

            // we only include the elements that are not wildcard'd
            def cpt = new LinkedHashMap()
            if(component.group() != request.group) {
                cpt.group = component.group()
            }
            if(component.name() != request.name) {
                cpt.name = component.name()
            }
            if(component.version() != request.version) {
                cpt.version = component.version()
            }

            // special case; if everything matches, then include all components, else get an empty map
            if(component.group() == request.group && component.name() == request.name && component.version() == request.version) {
                cpt.group = component.group()
                cpt.name = component.name()
                cpt.version = component.version()
            }
            componentList += cpt


            if(request.execute == "yes") {
                def assets = repoTx.browseAssets(component)
                assets.each { asset ->
                    log.info("delete-components: Deleting asset: " + asset.name())
                    repoTx.deleteAsset(asset)
                }
                def gav = [ group: component.group(), name: component.name(), version: component.version(), id: component.entityMetadata.id.value ]
                gavs += gav
                log.info("delete-components: Deleting component: ${gav.group} : ${gav.name} : ${gav.version}")
                repoTx.deleteComponent(component)
            }
        }
    }
    repoTx.commit()
} catch (Exception ex){
    log.warn("delete-components: ROLLBACK", ex)

    repoTx.rollback()

} finally {
    log.info("delete-components: DONE")
    repoTx.close()
}

gavs.each { gav ->
    mavenHostedFacet.deleteMetadata(gav.group, gav.name, gav.version)
    log.info("Deleting metadata for: " + gav.group + " : " + gav.name + " : " + gav.version)
    deletedVersions += gav.version
}

deletedVersions = deletedVersions.unique()

def result = JsonOutput.toJson([
        group: request.group,
        name: request.name,
        version: request.version,
        before: request.before,
        componentList: componentList,
        deleted: deletedVersions
])

return result