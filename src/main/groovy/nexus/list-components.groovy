import groovy.json.JsonOutput
import groovy.json.JsonSlurper
import org.sonatype.nexus.repository.storage.Bucket
import org.sonatype.nexus.repository.storage.StorageFacet

import java.util.regex.Matcher

def request = new JsonSlurper().parseText(args)
assert request.repo: 'repo parameter is required'


def repo = repository.repositoryManager.get(request.repo)
//log.info("repo: ${repo}")

StorageFacet storageFacet = repo.facet(StorageFacet)

//log.info("storageFacet: ${storageFacet}")

def repoTx = storageFacet.txSupplier().get()

//log.info("request.group  : ${request.group}")
//log.info("request.name   : ${request.name}")
//log.info("request.version: ${request.version}")

Date beforeDate = null
if(request.before) {
    Matcher matcher = request.before =~ /.*([0-9]{8}).*/
    if(matcher.matches()) {
        def datePart = matcher.group(1)
        try {
            beforeDate = Date.parse("yyyyMMdd", datePart)
        } catch (Exception ignore) {
            beforeDate = null
        }
    }
}
//log.info("beforeDate: ${beforeDate}")


def componentList = []

try {
    repoTx.begin()
    //log.info("repoTx: ${repoTx}")

    Bucket bucket = repoTx.findBucket(repo)
    //log.info("bucket: ${bucket}")

    def components = repoTx.browseComponents(bucket)

    components.each { component ->

        boolean include =
                   (request.group   == "" || component.group()   =~ /${request.group}/   ) &&
                   (request.name    == "" || component.name()    =~ /${request.name}/    ) &&
                   (request.version == "" || component.version() =~ /${request.version}/ )

        if(include && beforeDate != null) {
            Matcher matcher = component.version =~ /.*([0-9]{8}).*/
            if(matcher.matches()) {
                def datePart = matcher.group(1)
                try {
                    def componentDate = Date.parse("yyyyMMdd", datePart)
                    include = componentDate < beforeDate
                } catch (Exception ignored) {
                }
            }
        }

        if(include) {

            // we only include the elements that are not wildcard'd
            def cpt = new LinkedHashMap()
            if(component.group() != request.group) {
                cpt.group = component.group()
            }
            if(component.name() != request.name) {
                cpt.name = component.name()
            }
            if(component.version() != request.version) {
                cpt.version = component.version()
            }

            // special case; if everything matches, then include all components, else get an empty map
            if(component.group() == request.group && component.name() == request.name && component.version() == request.version) {
                cpt.group = component.group()
                cpt.name = component.name()
                cpt.version = component.version()
            }
            componentList += cpt
        }
    }
    repoTx.commit()
}
finally {
    repoTx.close()
}

def result = JsonOutput.toJson([
        group: request.group,
        name: request.name,
        version: request.version,
        before: request.before,
        componentList: componentList
])

return result