import groovy.json.JsonOutput
import groovy.json.JsonSlurper
import org.sonatype.nexus.blobstore.api.BlobStore
import org.sonatype.nexus.common.collect.AttributesMap
import org.sonatype.nexus.common.collect.NestedAttributesMap
import org.sonatype.nexus.common.hash.HashAlgorithm
import org.sonatype.nexus.repository.Format
import org.sonatype.nexus.repository.storage.*
import org.sonatype.nexus.repository.view.Content
import org.sonatype.nexus.repository.view.payloads.BlobPayload

import static java.util.Collections.singletonList
import static org.sonatype.nexus.repository.maven.internal.Attributes.P_ARTIFACT_ID
import static org.sonatype.nexus.repository.storage.ComponentEntityAdapter.P_VERSION
import static org.sonatype.nexus.repository.storage.MetadataNodeEntityAdapter.P_NAME

def request = new JsonSlurper().parseText(args)
assert request.fromRepo: 'fromRepo parameter is required'
assert request.toRepo: 'toRepo parameter is required'
assert request.version: 'version parameter is required'

def fromRepo = repository.repositoryManager.get(request.fromRepo)
def toRepo = repository.repositoryManager.get(request.toRepo)

log.info("promote-docker-version: fromRepo: ${request.fromRepo}")
log.info("promote-docker-version: toRepo  : ${request.toRepo}")
log.info("promote-docker-version: version : ${request.version}")

assert fromRepo.format.value == "docker": "fromRepo is not a 'docker' repo"
assert toRepo.format.value == "docker": "toRepo is not a 'docker' repo"

StorageFacet fromStorageFacet = fromRepo.facet(StorageFacet)
StorageFacet toStorageFacet = toRepo.facet(StorageFacet)

int numComponents = 0
int numAssets = 0

def fromTx = fromStorageFacet.txSupplier().get()
def toTx = toStorageFacet.txSupplier().get()

def DOCKER_ALGORITHMS = [HashAlgorithm.SHA1, HashAlgorithm.SHA256]

try {
    fromTx.begin()
    toTx.begin()

    Bucket fromBucket = fromTx.findBucket(fromRepo)
    Bucket toBucket = toTx.findBucket(toRepo)

    def fromComponents = fromTx.browseComponents(fromBucket)

    //
    // slight simplification of the steps for promoting maven versions
    //
    fromComponents.each { fromComponent ->

        if(fromComponent.version == request.version) {

            // name, eg: incodehq-public/simpleapp
            def gav = [ name: fromComponent.name(), version: fromComponent.version() ]

            def format = new Format(fromComponent.format()) {}

            final Iterable<Component> existingComponents = toTx.findComponents(
                    Query.builder()
                            .where(P_NAME).eq(gav.name)
                            .and(P_VERSION).eq(gav.version)
                            .build(),
                    singletonList(toRepo)
            )
            def isExistingComponent = existingComponents.iterator().hasNext()

            if (isExistingComponent) {
                log.info("promote-docker-version: component: ${gav.name} - SKIPPED")
            } else {
                // no such component
                log.info("promote-docker-version: component: ${gav.name}")

                def fromAssets = fromTx.browseAssets(fromComponent)

                Component toComponent = null
                NestedAttributesMap toComponentAttributes = null

                fromAssets.each { fromAsset ->

                    def fromAssetName = fromAsset.name()

                    log.info("promote-docker-version: asset.name: ${fromAssetName}")

                    def fromContentAttributes = new AttributesMap()
                    Content.extractFromAsset(fromAsset, DOCKER_ALGORITHMS, fromContentAttributes)

                    if(toComponent == null) {

                        toComponent = toTx.createComponent(toBucket, format)
                                .name(gav.name)
                                .version(gav.version)

                        toComponentAttributes = toComponent.formatAttributes()
                        toComponentAttributes.set(P_ARTIFACT_ID, gav.name)
                        toComponentAttributes.set(P_VERSION, gav.version)

                        toTx.saveComponent(toComponent)
                        numComponents++
                    }

                    Asset toAsset = toTx.findAssetWithProperty(P_NAME, fromAssetName, toBucket)
                    if (toAsset == null) {
                        toAsset = toTx.createAsset(toBucket, toComponent)

                        toAsset.name(fromAssetName)

                        NestedAttributesMap toAssetAttributes = toAsset.formatAttributes()

                        toAssetAttributes.set(P_ARTIFACT_ID, gav.name)
                        toAssetAttributes.set(P_VERSION, gav.version)

                        def fromBlobRef = fromAsset.blobRef()
                        def fromBlob = fromTx.getBlob(fromBlobRef)

                        Map<String, String> headers = fromBlob.getHeaders()

                        def blobName = headers.get(BlobStore.BLOB_NAME_HEADER)

                        log.info("promote-docker-version: asset.name: ${fromAssetName} ; blob.name: ${blobName}")
                        // remove these headers because they are auto-populated by createTempBlob (and Nexus checks that cannot be duplicated)
                        headers.remove("BlobStore.created-by-ip")
                        headers.remove("BlobStore.created-by")
                        headers.remove("BlobStore.content-type")
                        headers.remove("Bucket.repo-name")
                        headers.remove(BlobStore.BLOB_NAME_HEADER)
                        headers.each { k,v ->
                            log.info("promote-docker-version: asset.name: ${fromAssetName} ; blob.name: ${blobName}; header $k = $v ")
                        }


                        def declaredContentType = fromAsset.contentType()
                        def skipContentVerification = true

                        def fromBlobPayload = new BlobPayload(fromBlob, declaredContentType)
                        def tempBlob = toStorageFacet.createTempBlob(fromBlobPayload, DOCKER_ALGORITHMS)
                        try {
                            def toAssetBlob = toTx.createBlob(blobName, tempBlob, headers, declaredContentType, skipContentVerification)
                            toTx.attachBlob(toAsset, toAssetBlob)
                            def modified = Content.maintainLastModified(toAsset, fromContentAttributes)
                            Content.applyToAsset(toAsset, modified)

                        } finally {
                            tempBlob.close()
                        }
                        toAsset.markAsDownloaded()
                        toTx.saveAsset(toAsset)
                        numAssets++
                    }
                }
            }
        }
    }

    fromTx.commit()
    toTx.commit()
} catch (Exception ex){
    log.warn("promote-docker-version: ROLLBACK", ex)

    toTx.rollback()
    fromTx.rollback()
} finally {
    log.info("promote-docker-version: DONE")
    fromTx.close()
    toTx.close()
}

def result = JsonOutput.toJson([
    numComponents: numComponents,
    numAssets: numAssets
])

return result