import groovy.json.JsonOutput
import groovy.json.JsonSlurper
import org.apache.maven.model.Model
import org.sonatype.nexus.blobstore.api.BlobStore
import org.sonatype.nexus.common.collect.AttributesMap
import org.sonatype.nexus.common.collect.NestedAttributesMap
import org.sonatype.nexus.repository.Format
import org.sonatype.nexus.repository.maven.MavenFacet
import org.sonatype.nexus.repository.maven.MavenPath
import org.sonatype.nexus.repository.maven.MavenPathParser
import org.sonatype.nexus.repository.maven.internal.MavenModels
import org.sonatype.nexus.repository.storage.*
import org.sonatype.nexus.repository.view.Content
import org.sonatype.nexus.repository.view.payloads.BlobPayload

import static java.util.Collections.singletonList
import static org.sonatype.nexus.repository.maven.internal.Attributes.*
import static org.sonatype.nexus.repository.storage.AssetEntityAdapter.P_ASSET_KIND
import static org.sonatype.nexus.repository.storage.ComponentEntityAdapter.P_GROUP
import static org.sonatype.nexus.repository.storage.ComponentEntityAdapter.P_VERSION
import static org.sonatype.nexus.repository.storage.MetadataNodeEntityAdapter.P_NAME

def request = new JsonSlurper().parseText(args)
assert request.fromRepo: 'fromRepo parameter is required'
assert request.toRepo: 'toRepo parameter is required'
assert request.version: 'version parameter is required'

def fromRepo = repository.repositoryManager.get(request.fromRepo)
def toRepo = repository.repositoryManager.get(request.toRepo)

log.info("promote-maven-version: fromRepo: ${request.fromRepo}")
log.info("promote-maven-version: toRepo  : ${request.toRepo}")
log.info("promote-maven-version: version : ${request.version}")

assert fromRepo.format.value == "maven2": "fromRepo is not a 'maven2' repo"
assert toRepo.format.value == "maven2": "toRepo is not a 'maven2' repo"

MavenFacet fromMavenFacet = fromRepo.facet(MavenFacet)
MavenPathParser fromMavenPathParser = fromMavenFacet.mavenPathParser

StorageFacet fromStorageFacet = fromRepo.facet(StorageFacet)
StorageFacet toStorageFacet = toRepo.facet(StorageFacet)

int numComponents = 0
int numAssets = 0

def fromTx = fromStorageFacet.txSupplier().get()
def toTx = toStorageFacet.txSupplier().get()

try {
    fromTx.begin()
    toTx.begin()

    Bucket fromBucket = fromTx.findBucket(fromRepo)
    Bucket toBucket = toTx.findBucket(toRepo)

    def fromComponents = fromTx.browseComponents(fromBucket)

    //
    // modelled o the steps performed by MavenFacet#put(...)
    // in particular, MavenFacetImpl#putArtifact
    //
    fromComponents.each { fromComponent ->

        if(fromComponent.version == request.version) {

            def gav = [ group: fromComponent.group(), name: fromComponent.name(), version: fromComponent.version() ]

            def format = new Format(fromComponent.format()) {}

            final Iterable<Component> existingComponents = toTx.findComponents(
                    Query.builder()
                            .where(P_GROUP).eq(gav.group)
                            .and(P_NAME).eq(gav.name)
                            .and(P_VERSION).eq(gav.version)
                            .build(),
                    singletonList(toRepo)
            )
            def isExistingComponent = existingComponents.iterator().hasNext()

            if (isExistingComponent) {
                log.info("promote-maven-version: component: ${gav.group} : ${gav.name} - SKIPPED")
            } else {
                // no such component
                log.info("promote-maven-version: component: ${gav.group} : ${gav.name}")

                def fromAssets = fromTx.browseAssets(fromComponent)

                Component toComponent = null
                NestedAttributesMap toComponentAttributes = null

                fromAssets.each { fromAsset ->

                    def fromAssetName = fromAsset.name()
                    MavenPath mavenPath = fromMavenPathParser.parsePath(fromAssetName)
                    def coordinates = mavenPath.coordinates

                    log.info("promote-maven-version: asset.name: ${fromAssetName}")

                    def fromContentAttributes = new AttributesMap()
                    Content.extractFromAsset(fromAsset, MavenPath.HashType.ALGORITHMS, fromContentAttributes)

                    if(toComponent == null) {

                        toComponent = toTx.createComponent(toBucket, format)
                                .group(coordinates.groupId)
                                .name(coordinates.artifactId)
                                .version(coordinates.version)

                        toComponentAttributes = toComponent.formatAttributes()
                        toComponentAttributes.set(P_GROUP_ID, coordinates.groupId)
                        toComponentAttributes.set(P_ARTIFACT_ID, coordinates.artifactId)
                        toComponentAttributes.set(P_VERSION, coordinates.version)
                        toComponentAttributes.set(P_BASE_VERSION, coordinates.baseVersion)

                        toTx.saveComponent(toComponent)
                        numComponents++
                    }


                    Asset toAsset = toTx.findAssetWithProperty(P_NAME, mavenPath.path, toBucket)
                    if (toAsset == null) {
                        toAsset = toTx.createAsset(toBucket, toComponent)

                        toAsset.name(mavenPath.path)

                        NestedAttributesMap toAssetAttributes = toAsset.formatAttributes()

                        toAssetAttributes.set(P_GROUP_ID, coordinates.groupId)
                        toAssetAttributes.set(P_ARTIFACT_ID, coordinates.artifactId)
                        toAssetAttributes.set(P_VERSION, coordinates.version)
                        toAssetAttributes.set(P_BASE_VERSION, coordinates.baseVersion)
                        toAssetAttributes.set(P_CLASSIFIER, coordinates.classifier)
                        toAssetAttributes.set(P_EXTENSION, coordinates.extension)
                        toAssetAttributes.set(
                                P_ASSET_KIND,
                                mavenPath.isSubordinate()
                                        ? AssetKind.ARTIFACT_SUBORDINATE.name()
                                        : AssetKind.ARTIFACT.name()
                        )


                        def fromBlobRef = fromAsset.blobRef()
                        def fromBlob = fromTx.getBlob(fromBlobRef)

                        Map<String, String> headers = fromBlob.getHeaders()

                        def blobName = headers.get(BlobStore.BLOB_NAME_HEADER)

                        log.info("promote-maven-version: asset.name: ${fromAssetName} ; blob.name: ${blobName}")
                        // remove these headers because they are auto-populated by createTempBlob (and Nexus checks that cannot be duplicated)
                        headers.remove("BlobStore.created-by-ip")
                        headers.remove("BlobStore.created-by")
                        headers.remove("BlobStore.content-type")
                        headers.remove("Bucket.repo-name")
                        headers.remove(BlobStore.BLOB_NAME_HEADER)
                        headers.each { k,v ->
                            log.info("promote-maven-version: asset.name: ${fromAssetName} ; blob.name: ${blobName}; header $k = $v ")
                        }


                        def declaredContentType = fromAsset.contentType()
                        def skipContentVerification = true

                        def fromBlobPayload = new BlobPayload(fromBlob, declaredContentType)
                        def tempBlob = toStorageFacet.createTempBlob(fromBlobPayload, MavenPath.HashType.ALGORITHMS)
                        try {
                            def toAssetBlob = toTx.createBlob(blobName, tempBlob, headers, declaredContentType, skipContentVerification)
                            toTx.attachBlob(toAsset, toAssetBlob)
                            def modified = Content.maintainLastModified(toAsset, fromContentAttributes)
                            Content.applyToAsset(toAsset, modified)

                        } finally {
                            tempBlob.close()
                        }
                        toAsset.markAsDownloaded()
                        toTx.saveAsset(toAsset)
                        numAssets++

                        if (mavenPath.isPom()) {
                            //MavenFacetImpl#fillInFromModel(...)
                            Model model = MavenModels.readModel(fromBlob.inputStream)
                            if (model == null) {
                                log.warn("Could not parse POM: {} @ {}", toRepo.name, mavenPath.path)
                            }
                            String packaging = model.packaging
                            toComponentAttributes.set(P_PACKAGING, packaging == null ? "jar" : packaging)
                            toComponentAttributes.set(P_POM_NAME, model.name)
                            toComponentAttributes.set(P_POM_DESCRIPTION, model.description)

                            toTx.saveComponent(toComponent)
                        }
                    }
                }
            }
        }
    }

    fromTx.commit()
    toTx.commit()
} catch (Exception ex){
    log.warn("promote-maven-version: ROLLBACK", ex)

    toTx.rollback()
    fromTx.rollback()
} finally {
    log.info("promote-maven-version: DONE")
    fromTx.close()
    toTx.close()
}

def result = JsonOutput.toJson([
    numComponents: numComponents,
    numAssets: numAssets
])

return result