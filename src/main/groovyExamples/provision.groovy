import com.google.common.collect.Lists
import org.sonatype.nexus.blobstore.api.BlobStoreConfiguration
import org.sonatype.nexus.repository.maven.LayoutPolicy
import org.sonatype.nexus.repository.maven.VersionPolicy
import org.sonatype.nexus.repository.storage.WritePolicy
import org.sonatype.nexus.security.internal.SecurityApiImpl

// The four main API providers are blobStore, security, repository and core.

BlobStoreConfiguration store = blobStore.createFileBlobStore('npm', 'npm')

final SecurityApiImpl securityApiImpl = security
securityApiImpl.addRole('blue','blue', 'Blue Role', [], [])

repository.createBowerHosted('bower-internal')

core.baseUrl('http://repo.example.com')


repository.repositoryManager.get("maven-releases").delete()
repository.repositoryManager.get("maven-snapshots").delete()
repository.repositoryManager.get("maven-public").delete()

repository.repositoryManager.get("nuget-hosted").delete()
repository.repositoryManager.get("nuget-group").delete()

repository.createMavenHosted("maven-dev" , "maven-dev", true, VersionPolicy.MIXED, WritePolicy.ALLOW, LayoutPolicy.STRICT)
repository.createMavenHosted("maven-test", "maven-test", true, VersionPolicy.RELEASE, WritePolicy.ALLOW, LayoutPolicy.STRICT)
repository.createMavenHosted("maven-prod", "maven-prod", true, VersionPolicy.RELEASE, WritePolicy.ALLOW, LayoutPolicy.STRICT)

repository.createMavenGroup("maven", Lists.newArrayList("maven-prod", "maven-test", "maven-dev", "maven-central"))

repository.createDockerHosted("docker-dev", 5001, null, "docker-dev", true, true, WritePolicy.ALLOW)
repository.createDockerHosted("docker-test", 5002, null, "docker-test", true, true, WritePolicy.ALLOW)
repository.createDockerHosted("docker-prod", 5003, null, "docker-prod", true, true, WritePolicy.ALLOW)

repository.createDockerGroup("docker", 5000, null, Lists.newArrayList("docker-prod", "docker-test", "docker-dev", "maven-central"))
