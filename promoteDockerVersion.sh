#!/bin/bash

set -e
set -u

########################################

username=$INCODE_CLOUD_REPO_U
password=$INCODE_CLOUD_REPO_P
host=$INCODE_CLOUD_REPO_H

fromRepo=docker-dev
toRepo=docker-test

while getopts u:p:h:f:t:v: option
do
    case "${option}"
    in
        u) username=${OPTARG};;
        p) password=${OPTARG};;
        h) host=${OPTARG};;
        f) fromRepo=${OPTARG};;
        t) toRepo=${OPTARG};;
        v) version=${OPTARG};;
    esac
done

echo ""                     >&2
echo "host     : $host"     >&2
echo ""                     >&2
echo "fromRepo : $fromRepo" >&2
echo "toRepo   : $toRepo"   >&2
echo "version  : $version"  >&2
echo ""                     >&2


if [ "Z${username}Z" == "ZZ" ]; then
    echo "usage: username (-u) required"
    exit 1
fi
if [ "Z${password}Z" == "ZZ" ]; then
    echo "usage: password (-p) required"
    exit 1
fi
if [ "Z${host}Z" == "ZZ" ]; then
    echo "usage: host (-h) required"
    exit 1
fi

if [ "Z${fromRepo}Z" == "ZZ" ]; then
    echo "usage: fromRepo (-f) required"
    exit 1
fi
if [ "Z${toRepo}Z" == "ZZ" ]; then
    echo "usage: toRepo (-t) required"
    exit 1
fi
if [ "Z${version}Z" == "ZZ" ]; then
    echo "usage: version (-v) required"
    exit 1
fi

########################################

basename=$(basename $0)
tmpfile="/tmp/$basename.$$"

curl \
    -u $username:$password \
    -m 900 \
    --header 'Content-Type: text/plain' \
    --header 'Accept: application/json' \
    -d \
"{ \"fromRepo\": \"$fromRepo\", \"toRepo\": \"$toRepo\", \"version\": \"$version\" }" \
$host/service/rest/v1/script/promote-docker-version/run \
    -o $tmpfile 2>/dev/null

cat $tmpfile | jq --raw-output '.result'

########################################

rm $tmpfile



