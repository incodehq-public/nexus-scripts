#!/bin/bash

set -e
#set -u

########################################

username=$INCODE_CLOUD_REPO_U
password=$INCODE_CLOUD_REPO_P
host=$INCODE_CLOUD_REPO_H

while getopts u:p:h: option
do
    case "${option}"
    in
        u) username=${OPTARG};;
        p) password=${OPTARG};;
        h) host=${OPTARG};;
    esac
done

if [ "Z${username}Z" == "ZZ" ]; then
    echo "usage: username (-u) required"
    exit 1
fi
if [ "Z${password}Z" == "ZZ" ]; then
    echo "usage: password (-p) required"
    exit 1
fi
if [ "Z${host}Z" == "ZZ" ]; then
    echo "usage: host (-h) required"
    exit 1
fi

########################################

printf "Dropping from host: $host\n"

sh _nexusScriptDrop.sh \
        -n $script \
        -h $host \
        -u $username -p $password \
        delete-components promote-maven-version promote-docker-version list-components
