#!/bin/bash

set -e
set -u

########################################

username=$INCODE_CLOUD_REPO_U
password=$INCODE_CLOUD_REPO_P
host=$INCODE_CLOUD_REPO_H

while getopts u:p:h: option
do
    case "${option}"
    in
        u) username=${OPTARG};;
        p) password=${OPTARG};;
        h) host=${OPTARG};;
    esac
done
shift $((OPTIND-1))
scripts=$@


########################################

# add a script to the repository manager and run it
function addAndRunScript {
  name=$1
  file=$2
  # using grape config that points to local Maven repo and Central Repository , default grape config fails on some downloads although artifacts are in Central
  # change the grapeConfig file to point to your repository manager, if you are already running one in your organization
  groovy -Dgroovy.grape.report.downloads=true -Dgrape.config=grapeConfig.xml src/main/groovy/local/addUpdateScript.groovy -u "$username" -p "$password" -n "$name" -f "$file" -h "$host"

  #curl -v -X POST -u $username:$password --header "Content-Type: text/plain" "$host/service/rest/v1/script/$name/run"
  #printf "\nSuccessfully executed $name script\n\n\n"
}

########################################


for name in $scripts
do

    file=src/main/groovy/nexus/$name.groovy \

    addAndRunScript $name $file

    printf "\nUploaded Nexus script $name\n\n"
done

