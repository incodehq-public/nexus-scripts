#!/bin/bash

set -e
set -u

########################################

username=$INCODE_CLOUD_REPO_U
password=$INCODE_CLOUD_REPO_P
host=$INCODE_CLOUD_REPO_H

while getopts u:p:h: option
do
    case "${option}"
    in
        u) username=${OPTARG};;
        p) password=${OPTARG};;
        h) host=${OPTARG};;
    esac
done

shift $((OPTIND-1))
scripts=$@


########################################

function deleteScript {
  name=$1

  curl -v -X DELETE -u $username:$password "$host/service/rest/v1/script/$name"
  printf "\nDeleted script $name\n\n"
}

########################################

for name in $scripts
do
    deleteScript $name
done

printf "\nDropped Nexus Script: $name \n\n"
