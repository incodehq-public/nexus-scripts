#!/bin/bash

set -e
#set -u


########################################

while getopts u:p:h:f:t:n:v: option
do
    case "${option}"
    in
        u) username=${OPTARG};;
        p) password=${OPTARG};;
        h) host=${OPTARG};;
        f) fromRegistry=${OPTARG};;
        t) toRegistry=${OPTARG};;
        n) image=${OPTARG};;
        v) version=${OPTARG};;
    esac
done

echo ""                             >&2
echo "host         : $host"         >&2
echo ""                             >&2
echo "fromRegistry : $fromRegistry" >&2
echo "toRegistry   : $toRegistry"   >&2
echo "image        : $image"        >&2
echo "version      : $version"      >&2
echo ""                             >&2

if [ "Z${username}Z" == "ZZ" ]; then
    echo "usage: username (-u) required"
    exit 1
fi
if [ "Z${password}Z" == "ZZ" ]; then
    echo "usage: password (-p) required"
    exit 1
fi
if [ "Z${host}Z" == "ZZ" ]; then
    echo "usage: host (-h) required"
    exit 1
fi

if [ "Z${fromRegistry}Z" == "ZZ" ]; then
    echo "usage: fromRegistry (-f) required"
    exit 1
fi
if [ "Z${toRegistry}Z" == "ZZ" ]; then
    echo "usage: toRegistry (-t) required"
    exit 1
fi
if [ "Z${image}Z" == "ZZ" ]; then
    echo "usage: image (-n) required"
    exit 1
fi
if [ "Z${version}Z" == "ZZ" ]; then
    echo "usage: version (-v) required"
    exit 1
fi

########################################

docker pull $fromRegistry.$host/$username/$image:$version
docker tag $fromRegistry.$host/$username/$image:$version $toRegistry.$host/$username/$image:$version
docker push $toRegistry.$host/$username/$image:$version

#sh promoteDockerImage.sh -u incodehq-public -p xxx -h incode.cloud -f docker-dev -t docker-test -n simpleapp -v 2.0.0-M1.20180408-1423-fb88e0b3