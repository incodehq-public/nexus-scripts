#!/bin/bash

set -e
#set -u

########################################

username=$INCODE_CLOUD_REPO_U
password=$INCODE_CLOUD_REPO_P
host=$INCODE_CLOUD_REPO_H
repo=maven-dev
execute=no

while getopts u:p:h:r:g:n:v:b:x option
do
    case "${option}"
    in
        u) username=${OPTARG};;
        p) password=${OPTARG};;
        h) host=${OPTARG};;
        r) repo=${OPTARG};;
        g) group=${OPTARG};;
        n) name=${OPTARG};;
        v) version=${OPTARG};;
        b) before=${OPTARG};;
        x) execute=yes;;
    esac
done

echo ""                 >&2
echo "host   : $host"   >&2
echo "repo   : $repo"   >&2
echo ""                 >&2
echo "group  : $group"  >&2
echo "name   : $name"   >&2
echo "version: $version">&2
echo "before : $before" >&2
echo "execute: $execute" >&2
echo ""                 >&2

if [ "Z${username}Z" == "ZZ" ]; then
    echo "usage: username (-u) required"
    exit 1
fi
if [ "Z${password}Z" == "ZZ" ]; then
    echo "usage: password (-p) required"
    exit 1
fi
if [ "Z${host}Z" == "ZZ" ]; then
    echo "usage: host (-h) required"
    exit 1
fi

if [ "Z${repo}Z" == "ZZ" ]; then
    echo "usage: repo (-r) required"
    exit 1
fi

if [ "Z${name}Z" == "ZZ" -a "Z${version}Z" == "ZZ" -a "Z${before}Z" == "ZZ" ]; then
    echo "usage: require at least one of 'name' (-n), 'version' (-v) and 'before' (-b)" >&2
    exit 1
fi

########################################

basename=$(basename $0)
tmpfile="/tmp/$basename.$$"

curl \
    -u $username:$password \
    -m 900 \
    --header 'Content-Type: text/plain' \
    --header 'Accept: application/json' \
    -d \
"{ \"repo\": \"$repo\", \"group\": \"$group\", \"name\": \"$name\", \"version\": \"$version\", \"before\": \"$before\", \"execute\": \"$execute\" }" \
$host/service/rest/v1/script/delete-components/run \
    -o $tmpfile 2>/dev/null

echo ""
echo "components:"
cat $tmpfile | jq --raw-output '.result' | jq '.componentList'

echo ""
echo "deleted:"
cat $tmpfile | jq --raw-output '.result' | jq '.deleted'

########################################

rm $tmpfile

